# Hamed Mirzaei
# telegram  ID --> @HamedMirzaei_Official
# instagram ID --> HamedMirzaei2001Official
# email --> HamedMirzaei2001Official@gmail.com

from user import User
from random import choice
from estate import Apartment, House, Store
from region import Region
from advertisment import ApartmentSell, HouseSell, ApartmentRent
from manager import Manager

FIRST_NAME = ['james', 'ian', 'cris']
LAST_NAME = ['russel', 'grace', 'morgan']
MOBILES = ['09334519059', '09336781920', '09301045025', '09046363876'
    , '09187157914',]

if __name__ == "__main__":
    for mobile in MOBILES:
        User(choice(FIRST_NAME), choice(LAST_NAME), mobile)

    reg = Region('text')
    aps1 = ApartmentSell(has_elevator=True, has_parking=True, floor=2
                         , user=User.objects_list[3], area=127, rooms_count=3
                         , built_year=1390, region=reg, address= 'some'
                         , price_per_meter=1000, discountable=True
                         , convertable=False)

    aps2 = ApartmentSell(has_elevator=True, has_parking=True, floor=1
                         , user=User.objects_list[3], area=124, rooms_count=3
                         , built_year=1390, region=reg, address='some'
                         , price_per_meter=100000, discountable=True
                         , convertable=False)

    aps3 = ApartmentSell(has_elevator=False, has_parking=False, floor=2
                         , user=User.objects_list[3], area=120, rooms_count=4
                         , built_year=1390, region=reg, address='some'
                         , price_per_meter=100, discountable=True
                         , convertable=False)

    search_result1 = ApartmentSell.manager.search(price_per_meter__max=999)
    print(search_result1)
    search_result2 = ApartmentSell.manager.get(price_per_meter=1000)
    print(search_result2)