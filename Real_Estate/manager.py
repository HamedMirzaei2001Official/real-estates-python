# Hamed Mirzaei
# telegram  ID --> @HamedMirzaei_Official
# instagram ID --> HamedMirzaei2001Official
# email --> HamedMirzaei2001Official@gmail.com

class Manager:

    def __init__(self, _class=None):
        self._class = _class

    def __str__(self):
        return f'Manager {self._class}'

    def search(self, **kwargs):
        results = list()
        for key, value in kwargs.items():
            if key.endswith('__min'):
                key = key[:-5] # returns key to first mood
                compare_key = 'min'
            elif key.endswith('__max'):
                key = key[:-5]
                compare_key = 'max'
            else:
                compare_key = 'equal'

            for obj in self._class.objects_list:
                if hasattr(obj, key): #check obj have a key like this key
                    if compare_key == 'min':
                        result = bool(getattr(obj, key) >= value)
                        # result = True or False
                    elif compare_key == 'max':
                        result = bool(getattr(obj, key) <= value)
                    else:
                        result = bool(getattr(obj, key) == value)

                    if result: #if result == True
                        results.append(obj)

        return results #list of objects

    #
    # def get(self, **kwargs): #this function just returns one object
    #     for key, value in kwargs.items():
    #         result = list()
    #         for obj in self._class.objects_list():
    #             if hasattr(obj, key) and getattr(obj, key) == value:
    #                 return obj
    #         return None
    def get(self, **kwargs):
        for key, value in kwargs.items():
            for obj in self._class.objects_list:
                if hasattr(obj, key) and getattr(obj, key) == value:
                    return obj
        return None

    def __str__(self):
        return f"Manager {self._class}"