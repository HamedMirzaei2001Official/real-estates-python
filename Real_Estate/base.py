# Hamed Mirzaei
# telegram  ID --> @HamedMirzaei_Official
# instagram ID --> HamedMirzaei2001Official
# email --> HamedMirzaei2001Official@gmail.com

from abc import ABC
from manager import Manager

class BaseClass(ABC):
    """
    each child class have a version of BaseClass for itself
    because of this point, when we create a object of Apartment and After that
    we create a object of House , both object have a same id thats means counter
    start in 1 for each class.

    """
    _id = 0
    objects_list = None #we want a object_list for each child class, so we should
    #initial object_list in store function ,that means when a object create in
    #a each class, class have a object_list for itself
    #finaly, each class have a defferent object_list for itself

    manager = None

    def __init__(self, *args, **kwargs):
        self.id = self.generate_id()
        self.store(self)
        self.set_manager()
        super().__init__(*args, **kwargs)

    @classmethod
    def generate_id(cls):
        cls._id += 1
        return cls._id


    @classmethod
    def set_manager(cls):
        if cls.manager is None:
            cls.manager = Manager(cls)

    @classmethod
    def store(cls, obj):
        if cls.objects_list is None:
            cls.objects_list = list()

        cls.objects_list.append(obj)
