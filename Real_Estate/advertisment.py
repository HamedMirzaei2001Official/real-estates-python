# Hamed Mirzaei
# telegram  ID --> @HamedMirzaei_Official
# instagram ID --> HamedMirzaei2001Official
# email --> HamedMirzaei2001Official@gmail.com


from estate import Apartment, House, Store
from deal import Sell, Rent
from base import BaseClass
class ApartmentSell(BaseClass, Apartment, Sell):

    def show_details(self):
        self.show_description()
        self.show_price()


class ApartmentRent(BaseClass, Apartment, Rent):
    pass


class HouseSell(BaseClass, House, Sell):
    def show_details(self):
        self.show_description()
        self.show_price()

class HouseRent(BaseClass, House, Rent):
    def show_details(self):
        self.show_description()
        self.show_price()


class StoreSell(BaseClass, Store, Sell):
    def show_details(self):
        self.show_description()
        self.show_price()



class StoreRent(BaseClass, Store, Rent):
    def show_details(self):
        self.show_description()
        self.show_price()


