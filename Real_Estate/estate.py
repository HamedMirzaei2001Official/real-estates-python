# Hamed Mirzaei
# telegram  ID --> @HamedMirzaei_Official
# instagram ID --> HamedMirzaei2001Official
# email --> HamedMirzaei2001Official@gmail.com

from abc import abstractmethod, ABC

class EstateAbstract(ABC):
    def __init__(self, user, region, area="unknown", rooms_count=4
                 , built_year=2019, address = "unknown", *args, **kwargs):
        self._user = user
        self._area = area
        self._room_count = rooms_count
        self._built_year = built_year
        self._region = region
        self._address = address
        super().__init__(*args, **kwargs)

    @abstractmethod
    def show_description(self):
        pass


class Apartment(EstateAbstract):
    def __init__(self, has_elevator=True, has_parking=True, floor=4, *args, **kwargs):
        self.has_elevator = has_elevator
        self.has_parking = has_parking
        self.floor = floor
        super().__init__(*args, **kwargs)

    def show_description(self):
        print(f"Apartment {self.id}\t area: {self.area}")


class House(EstateAbstract):
    def __init__(self, has_yard=True, floors_count=4, *args, **kwargs):
        self.has_yard = has_yard
        self.floors_count = floors_count
        super().__init__(*args, **kwargs)

    def show_description(self):
        print(f"House {self.id}")


class Store(EstateAbstract):
    #we don't need constructor because we don't have any atribiute for Store
    def show_description(self):
        print(f"Store {self.id}")