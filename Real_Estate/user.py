# Hamed Mirzaei
# telegram  ID --> @HamedMirzaei_Official
# instagram ID --> HamedMirzaei2001Official
# email --> HamedMirzaei2001Official@gmail.com

from base import BaseClass

class User(BaseClass):
    def __init__(self, first_name="unknown", last_name="unknown"
                 , phone_number="0932131221", *args, **kwargs):
        self.first_name = first_name
        self.last_name = last_name
        self.phone_number = phone_number
        super().__init__(*args, **kwargs)

    @property
    def full_name(self):
        return f"{self.first_name} {self.last_name}"
