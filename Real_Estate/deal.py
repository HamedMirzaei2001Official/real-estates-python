# Hamed Mirzaei
# telegram  ID --> @HamedMirzaei_Official
# instagram ID --> HamedMirzaei2001Official
# email --> HamedMirzaei2001Official@gmail.com

from abc import ABC


class Sell(ABC):
    def __init__(self, price_per_meter=2000, discountable=True, convertable=True
                 , *args, **kwargs):
        self.price_per_meter = price_per_meter
        self.discountable = discountable
        self.convertable = convertable
        super().__init__(*args, *kwargs)

    def show_price(self):
        print(f"price: {self.price_per_meter}\tdiscount: {self.discountable}"
              f"\tconvert: {self.convertable}")


class Rent(ABC):
    def __init__(self, initial_price=100, monthly_price=100, convertable=True
                 , discountable=True, *args, **kwargs):
        self.initial_price = initial_price
        self.monthly_price = monthly_price
        self.convertable = convertable
        self.discountable = discountable
        super().__init__(*args, **kwargs)