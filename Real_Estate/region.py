# Hamed Mirzaei
# telegram  ID --> @HamedMirzaei_Official
# instagram ID --> HamedMirzaei2001Official
# email --> HamedMirzaei2001Official@gmail.com

from base import BaseClass

class Region(BaseClass):
    def __init__(self, name="unknown", *args, **kwargs):
        self.name = name
        super().__init__(*args, **kwargs)